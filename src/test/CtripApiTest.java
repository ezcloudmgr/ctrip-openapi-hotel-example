package test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.junit.Test;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import com.ctrip.OTA_PingStub;
import com.ctrip.OTA_PingStub.Request;
import com.ctrip.OTA_PingStub.RequestResponse;
import com.ctrip.openapi.java.utils.SignatureUtils;

public class CtripApiTest {

	@Test
	public void pingTest() {
		try {
			OTA_PingStub stub = new OTA_PingStub();
			Request r = new Request();
			r.setRequestXML(readFile("ping.xml"));

			RequestResponse resp = stub.request(r);

			System.out.println(prettyXML(resp.getRequestResult()));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	String readFile(String fileName) throws IOException,
			NoSuchAlgorithmException {

		/*
		 * Init Velocity
		 */
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class",
				ClasspathResourceLoader.class.getName());
		ve.init();

		/*
		 * 變數設定
		 */
		VelocityContext context = new VelocityContext();
		// 联盟ID
		String aid = "22213";
		// 站点ID
		String sid = "456017";
		// 密钥KEY
		String apiKey = "97BE8CC5-F3E5-43C0-8B3E-488E155767CF";
		// Timestamp
		long timest = SignatureUtils.GetTimeStamp();
		// API分類
		String reqType = "OTA_Ping";
		// 验证码
		String md5 = SignatureUtils.CalculationSignature("" + timest, aid,
				apiKey, sid, reqType);

		context.put("AID", aid);
		context.put("SID", sid);
		context.put("TIME", timest);
		context.put("MD5", md5);
		context.put("REQUEST_TYPE", reqType);

		/*
		 * 合併
		 */
		Template t = ve.getTemplate("templates/ping.xml");
		StringWriter sw = new StringWriter();
		t.merge(context, sw);

		return sw.toString();
	}

	public String prettyXML(String xml) {

		try {
			final InputSource src = new InputSource(new StringReader(xml));
			final Node document = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder().parse(src).getDocumentElement();
			final Boolean keepDeclaration = Boolean.valueOf(xml
					.startsWith("<?xml"));

			// May need this:
			// System.setProperty(DOMImplementationRegistry.PROPERTY,"com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl");

			final DOMImplementationRegistry registry = DOMImplementationRegistry
					.newInstance();
			final DOMImplementationLS impl = (DOMImplementationLS) registry
					.getDOMImplementation("LS");
			final LSSerializer writer = impl.createLSSerializer();

			writer.getDomConfig().setParameter("format-pretty-print",
					Boolean.TRUE); // Set this to true if the output needs to be
									// beautified.
			writer.getDomConfig().setParameter("xml-declaration",
					keepDeclaration); // Set this to true if the declaration is
										// needed to be outputted.

			return writer.writeToString(document);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
